# ARM

ARM is a CPU architecture. It's very popular.


# Challenges

Many of the challenges that may end up here will be ARM, or at least have
some ARM binaries.

# Tools

## Disassembly/RE

Basically any modern tools will work on ARM, so Ghidra, IDA, radare2, etc.

If you want to get super basic with a literal disassembler, you can use
an ARM objdump. Something like ```arm-none-eabi-objdump```.

## Build Tools

Basically all of these challenges will be Linux or without a system.

That in mind, for arm, I suggest having ```arm-linux-genueabi``` toolchain
available. I personally installed ```gcc-arm-linux-gnueabi``` and
```g++-arm-linux-gnueabi``` and ```gcc-arm-none-eabi```.

## Emulation Tools

I suggest ```qemu-arm```. That's kind of the standard for this kind of thing.

Basically install ```qemu-user``` through apt (or whatever package manager) 
_and_ whichever toolchain you'd like to emulate. IE, if you'd like to emulate
```firstPass``` on a non-ARM, fresh Ubuntu, you could run something like:

```sh
sudo apt-get install gcc-arm-linux-gnueabi qemu-user

qemu-arm -L /usr/arm-linux-gnueabi /path/to/firstPass /path/to/crc_matched.bin
```

