# firstPass

This is the first challenge ronman has written here.

This is a reverse engineering challenge. This is not code execution.

Figure out what the executable wants and make it happy. Not happy enough
to execute code for you, just happy enough to tell you you got the flags.

This is an arm-linux executable.

This file should work with a command like:
```sh
qemu-arm -L /usr/arm-linxu-gnueabi ./firstPass <input-file>
```

The only input to this challenge is a file whose path is provided
as the only argument to the executable.

There are multiple challenges in this one executable, though the solving the
first earns you a success status in my book.

Be on the lookout for an official writeup on this one...


